from django.apps import AppConfig
import requests


class IrisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'iris'

    def ready(self):
        """
        Iris dataset will be downloaded as soon as app starts
        """
        url = "https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv"
        response = requests.get(url)
        if response.status_code == 200:
            with open("iris/datasets/iris.csv", "w") as fp:
                fp.write(response.text)
            print("Iris ready!")
        else:
            print("Error happened when downloading the iris csv data")