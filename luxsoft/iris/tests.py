from django.test import TestCase, Client
from django.urls import reverse

from .views import sepal_length, describe


class IrisTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.n = 5
        self.result = {
            "setosa": 28,
            "versicolor": 3,
            "virginica": 1
        }

    def test_sepal_length(self):
        """ Test case for sepal_length """
        response = self.client.get(reverse(sepal_length, args=[self.n]))
        result = response.json().get("result")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.result, result)

    def test_describe(self):
        """
        Test case for describe
        We have to calculate the values manually
        and then after we put them here
        """
        response = self.client.get(reverse(describe))
        result = response.json()
        count = int(result["sepal_length"]["count"])
        sepal_length_mean = round(result["sepal_length"]["mean"], 2)
        sepal_width_min = int(result["sepal_width"]["min"])
        petal_length_max = result["petal_length"]["max"]
        petal_width_std = round(result["petal_width"]["std"], 2)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count, 150)
        self.assertEqual(sepal_length_mean, 5.84)
        self.assertEqual(sepal_width_min, 2)
        self.assertEqual(petal_length_max, 6.9)
        self.assertEqual(petal_width_std, 0.76)
        