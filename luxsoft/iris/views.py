from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import pandas as pd


def sepal_length(request, n):
    """ 
    =====================
    Iris Sepal Length Comparison View
    =====================
    This function reads CSV into a Pandas Dataframe and then using an aggregate
    function it filters out the records by their species which has a 
    smaller sepal length to the given value n.
    Returns a json response.
    """
    df = pd.read_csv("iris/datasets/iris.csv")
    df2 = df[df["sepal_length"] <= n].groupby(['species']).count()
    d = df2["sepal_length"].to_dict()
    result = {
        "description": f"number of species that has smaller sepal length than {n}",
        "result": d
    }
    return JsonResponse(result)

def describe(request):
    """ 
    =====================
    Iris Dataset Describe View
    =====================
    This function reads CSV into a Pandas Dataframe and then simply
    returns basic statistics about it.
    Returns a json response.
    """
    df = pd.read_csv("iris/datasets/iris.csv") 
    result = df.describe().to_dict()
    return JsonResponse(result)