from django.urls import path
from .views import sepal_length, describe

urlpatterns = [
    path('sepal_length/<int:n>', sepal_length, name="sepal_length"),
    path('describe/', describe, name="describe"),
]