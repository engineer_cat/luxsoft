from django.shortcuts import render


def index(request):
    view_list = [
        "sequence/elem/13",
        "sequence/longest/13",
        "iris/sepal_length/5",
        "iris/describe",
    ]
    context = {'view_list': view_list}
    return render(request, 'luxsoft/index.html', context)
