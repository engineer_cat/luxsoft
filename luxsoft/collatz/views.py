from django.shortcuts import render
from django.http import HttpResponse
import copy


def collatz(n):
    """
    =====================
    Collatz Sequence
    =====================
    The following iterative sequence is defined for the set of positive integers:
    n → n/2 (n is even)
    n → 3n + 1 (n is odd)
    Using the rule above and starting with 13, we generate the following sequence:
    13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
    for given n it yields the list of values of the sequence
    (As a generator function)
    """
    yield n
    while(n != 1):
        n = n // 2 if n % 2 == 0 else 3*n + 1
        yield n

def collatz_longest_chain(n):
    """
    =====================
    Collatz Longest Chain View
    =====================
    For n=13 we know collatz sequence will return as:
        13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
    But this function will return longest chain smaller than 13:
        8 → 4 → 2 → 1 (eg. this chain is longer than 10 → 5)
    for given n it returns the list of values smaller than n which has the biggest array size
    """
    l = collatz(n)
    result = []
    temp = []
    flag = False
    length = 0
    for i in l:
        if i < n:
            if not flag:
                flag = True
            temp.append(i)
        elif flag and i > n:
            if len(temp) > length:
                result = copy.deepcopy(temp)
                length = len(temp)
            flag = False
            temp = []
        # print(i, temp, result, length, flag)
    if i == 1:
        if len(temp) > length:
            result = copy.deepcopy(temp)
            length = len(temp)
        # print(i, temp, result, length, flag)
    return result

def collatz_elem_view(request, n):
    """
    Returns a simple HTTP response of collatz sequence for the given n.
    """
    result = collatz(n)
    str_result = ", ".join([str(s) for s in result])
    return HttpResponse("Returns [ %s ] for %s" % (str_result, n))

def collatz_longest_view(request, n):
    """
    Returns a simple HTTP response of a list from 
    the collatz sequence that has smaller values from the given n
    and has the biggest array size (the longest chain of values).
    """
    result = collatz_longest_chain(n)    
    str_result = ", ".join([str(s) for s in result])
    return HttpResponse("Returns [ %s ] for %s" % (str_result, n))