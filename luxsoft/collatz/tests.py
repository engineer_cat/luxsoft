import pytest
import json

from django.test import TestCase, Client
from django.urls import reverse

from .views import (
    collatz_elem_view,
    collatz_longest_view,
    collatz,
    collatz_longest_chain,
)


class CollatzTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.n = 13
        self.result = [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
        self.longest_chain = [8, 4, 2, 1]

    def test_collatz_elem_view(self):
        """ Test case for collatz_elem_view """
        response = self.client.get(reverse(collatz_elem_view, args=[self.n]))
        result = response.content.decode()
        str_result = ", ".join([str(s) for s in self.result])
        str_result = "Returns [ %s ] for %s" % (str_result, self.n)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str_result, result)

    def test_collatz_longest_view(self):
        """ Test case for collatz_longest_view """
        response = self.client.get(reverse(collatz_longest_view, args=[self.n]))
        result = response.content.decode()
        str_result = ", ".join([str(s) for s in self.longest_chain])
        str_result = "Returns [ %s ] for %s" % (str_result, self.n)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str_result, result)


@pytest.mark.parametrize("n, expected_result", [
    (13, [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]),
    (26, [26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]),
    (6, [6, 3, 10, 5, 16, 8, 4, 2, 1]),
])
def test_collatz(n, expected_result):
    assert list(collatz(n)) == expected_result


@pytest.mark.parametrize("n, expected_result", [
    (13, [8, 4, 2, 1]),
    (26, [20, 10, 5, 16, 8, 4, 2, 1]),
    (6, [4, 2, 1]),
])
def test_collatz_longest_chain(n, expected_result):
    assert list(collatz_longest_chain(n)) == expected_result
