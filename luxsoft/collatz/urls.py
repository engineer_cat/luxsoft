from django.urls import path
from .views import collatz_elem_view, collatz_longest_view

urlpatterns = [
    path('elem/<int:n>', collatz_elem_view, name="elem"),
    path('longest/<int:n>', collatz_longest_view, name="longest"),
]