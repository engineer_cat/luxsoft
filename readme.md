# Luxsoft Assingment

In the assignment 2 different tasks were given. Therefore, I created two different django-app for each one of them.

## Quick start
```bash
cd luxsoft
pip install -r requirements.txt
python manage.py runserver
```

## Tests
```bash
pytest -v
```